# CryoAlert Platform Documentation

This documentation repository will provide a high-level description of the CryoAlert platform architecture and design. For a detailed description of each components, see the documentation of that component in its repository.

## Chapters

- [Deployment Options](./deployment_options/index.md)
- [Components](./components/index.md)
- [Data Flow](./data_flow/index.md)
- [Operations](./operations/index.md)
