# Advanced Standalone Deployment within a Local Network

A more advanced deployment within a local network is an extension of the [simple deployment](simple_local.md). It most closely mimics the [advanced deployment with segregated networks](advanced_segregated.md) in terms of used components, yet still relies on the fact that all components _must_ be in the same network. The deployment architecture is shown in the diagram below.

It should be noted here that, within CryoAdmin, the local server will be referred to as a "relay", as it functions as both a relay and a central server in this deployment option.

In the diagram, the dashboard sends alerts to the Telegram cloud. However, this is optional. Grafana (the dashboard) can be configured to send alerts to many channels. See its [documentation](https://grafana.com/docs/grafana/latest/alerting/configure-notifications/manage-contact-points/#list-of-supported-integrations) for the options and their details. If Telegram is not used, then CryoGram is not needed in this deployment.

In fact, it is not necessary to use Grafana as the dashboard technology at all. The system administrator is free to choose their prefered dashboard technology, as nothing depends on this choice.

![advanced_standalone](../img/advanced_local.png)
