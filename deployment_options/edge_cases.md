# Edge cases

As with any (software) project, there are always edge cases. This document describes some that have occured during development and deployment at QuTech.

[[_TOC_]]

## Unreachable Fridge PC

It can occur that a Fridge PC is unreachable; for example, it is not connected to any network because it still runs Windows 7. In this case, it is recommended to install CryoAgent on an auxiliary PC (for example, a measurement PC) and create a local network between the auxiliary PC and the Fridge PC. This way, the latter does not need to be connected to the internet while the CryoAgent installed on the former retrieves the data via the local network.

The Auxilary PC is (/should be) connected to the network where it can be scraped by Prometheus. This makes this solution transparent to the CryoAlert Platform. In general, it is not important where CryoAgent is installed, as long as it can retrieve the data from the fridge.

![edge_case](../img/edge_case.png)

## Single Connection Protocols

In the research labs, it is quite common to find equipment that _does_ have a method of communicating with the device, but only a single connection at a time is allowed. In many cases this is enough, but often these instruments are also controlled via research software to control the device for the experiments. It would be a shame if for this reason a fridge (or part of its control hardware) could not be monitored with CryoAlert. To solve this problem, a Python application called [TCP Dispatcher](https://gitlab.tudelft.nl/qutech-qdlabs/cryoalert/tcp-dispatcher) was created. This application allows for dispatching/routing incoming TCP requests to different protocols and allows for multiple connections to protocols that otherwise would not allow it. See its documentation for more details.
