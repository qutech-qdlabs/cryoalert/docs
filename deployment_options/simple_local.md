# Simple Deployment within a Local Network

In its most simple configuration, within a local network, only a few components are necessary to create a CryoAlert system. These components are drawn in the architecture diagram below. It is noted that for this setup, CryoAdmin, CryoRelay, and CryoGram are not necessary. This is because CryoAgent has the capability to load its alert definitions from a file, and Prometheus can load its targets from a file as well. This is the most manual solution, but also the most simple.

In the diagram, the dashboard sends alerts to the Telegram cloud. However, this is optional. Grafana (the dashboard) can be configured to send alerts to many channels. See its [documentation](https://grafana.com/docs/grafana/latest/alerting/configure-notifications/manage-contact-points/#list-of-supported-integrations) for the options and their details.

In fact, it is not necessary to use Grafana as the dashboard technology at all. The system administrator is free to choose their prefered dashboard technology, as nothing depends on this choice.

![simple_local](../img/simple_local.png)
