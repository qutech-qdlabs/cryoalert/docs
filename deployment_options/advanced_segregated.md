# Advanced Deplyoment with Segregated Networks

The most advanced deployment allows storing data of multiple fridges, potentially across multiple different networks (for example, in case of different organizations sharing the same deployment) in a single location, with a centralized dashboard application for all users and organizations.

Unique about this deployment option is the requirement for a so-called "relay PC". This PC runs a Prometheus agent that scrapes the CryoAgent data and forwards it to a Prometheus Server running outside the network of the CryoAgents. This "relay PC" can be simply an old laptop or even a fridge PC itself. However, it has to be noted that a shutdown or failure of the relay PC will not trigger any alert. So, if you run the relay on the fridge PC, then no alert will be given when the fridge PC has a failure or is shutdown.

Furthermore, it good to note that it is possible to deploy an instance of the Prometheus server per organization, to keep their data seperate, should they want this option.

In the diagram, the dashboard sends alerts to the Telegram cloud. However, this is optional. Grafana (the dashboard) can be configured to send alerts to many channels. See its [documentation](https://grafana.com/docs/grafana/latest/alerting/configure-notifications/manage-contact-points/#list-of-supported-integrations) for the options and their details. If Telegram is not used, then CryoGram is not needed in this deplyoment.

In fact, it is not necessary to use Grafana as the dashboard technology at all. The system administrator is free to choose their prefered dashboard technology, as nothing depends on this choice.

![advanced_segregated](../img/advanced_segregated.png)
