# CryoAlert Platform Deployment Options

CryoAlert is designed in such a way that multiple deployment options are possible. Which deployment option you choose is entirely up to you and dependent on your network structure. It is, of course, always possible to deviate from any of the options described here.

It is noted that each deployment contains multiple `components` that combine to create the specific deployment option. For more details on each component, see their [documentation](../components/index.md).

## Chapters

- [Simple Deployment within a Local Network](simple_local.md)
- [Advanced Deployment within a Local Network](advanced_local.md)
- [Advanced Deployment with Segregated Networks](advanced_segregated.md)
- [Edge Cases](edge_cases.md)
