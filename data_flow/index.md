# Data Flow Documentation

Within the CryoAlert Platform, several data flows exists, with data flowing through multiple [components](../components/index.md), to make all components perform their function. To better understand the flow of data, it is best to explain them visually and with examples. This is the purpose of the chapters mentioned below.

## Chapters

- [Service Discovery](service_discovery.md)
- [Metrics](metrics.md)
- [Alert Rules](alert_rules.md)
- [Telegram Users](telegram_users.md)
