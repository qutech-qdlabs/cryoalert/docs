# Telegram Data Flow

CryoGram adds some simple features to CryoAlert, but to do so, it requires some interaction with CryoAdmin. This document will explain the various interactions they have. Do note, that CryoGram authenticates to CryoAdmin via an API-Key that can be obtained from the CryoAdmin web interface.

## User Authentication

For each command that is received by CryoAlertBot, it will first check if the user sending the command is allowed to interact with CryoAlert via Telegram. For this, is will send a request to CryoAdmin to check if the user is known. This is done by linking the Telegram username to a user account within CryoAdmin. This sequence is captured in the diagram below.

![data_flow_telegram_is_allowed](../img/data_flow_telegram_is_allowed.png)

## Known Fridges

If the user is allowed to interact with CyoGram, it can request a list of the known fridges the user is allowed to access (this is determined by the organization the user and fridges belong to). When CryoAlert bot receives such a request, it will forward the request to CryoAdmin with the username of the sender. CryoAdmin will then retrieve the list of fridges the corresponding user is allowed to access from its database, and return that to CryoGram which in turn returns it to the user via a message.

## Subscribing

Once the user knows which fridges they can access, they can subscribe to those fridges by sending the subscribe command to CryoAlertBot. It will then retrieve the chatID of the group chat that belongs to that fridge from CryoAdmin, from which is can construct an invite link (using Telegram built-in features). This is then returned to the user via a message. The telegram client automatically formats the invite link in a nice format so the user clearly understands what kind of link it received.

![data_flow_subscribe](../img/data_flow_subscribe.png)
