# Metrics Data Flow

The primary data flow is the data flow regarding the metrics, which contain the data as obtained by each CryoAgent. Additionally, the metrics also contain the alert settings for each fridge. As shown in the sequence diagram below, it can be roughly separated in three parts.

![data_flow_metrics](../img/data_flow_metrics.png)

## Data Acquisition Loop

The data acquisition loop is the responsibility and main function of CryoAgent. Based on its configuration, it will retrieve data from a data source and store it within its local database.

## Scrape Loop

The scrape loop is executed by a Prometheus instance (either an agent or a server) and fetches the data from multiple CryoAgent instances. The data is then stored in a local datastore for further use.

## Remote Write Loop

The remote write loop occurs only when both a Prometheus agent and server are present. When agents are present, this loop writes the locally scraped data to the central server where it can be queried for use in a dashboard.
