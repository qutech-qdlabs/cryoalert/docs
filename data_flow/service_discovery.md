# Service Discovery Data Flow

Prometheus supports many service discovery protocols. When CryoAdmin is used, it is possible to use HTTP service discovery with CryoAdmin as the server. This allows Prometheus to obtain a list of targets from CryoAdmin, based on the configured fridges for the relay this Prometheus instance belongs to.

![data_flow_sd](../img/data_flow_sd.png)

Note that the request requires an API-Key in the header. This is used to authenticate the relay(PC) that performs this requests. This API-Key is obtained from CryoAdmin and used by Prometheus via its configuration file. The response will be in the following format:

```json
[
 {
  "targets": [
   "localhost:8081"
  ],
  "labels": {
   "fridge": "testfridge",
   "organization": "default",
   "extra_label1": "bla",
   ...
  }
 },
 {
  ...
 }
]
```
