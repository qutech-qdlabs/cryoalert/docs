# Alert Rule and Response Updates Data Flow

The sequence of requests and data flow when updating alert rules for a specific fridge, and letting CryoAdmin know if such an update has been successful, involves multiple components and steps between them. Therefore, it is informative to describe and visualize the steps in a sequence diagram, as shown below.

![data_flow_alerts](/img/data_flow_alerts.png)

As can be seen in the diagram, the sequence starts when the CryoRelay polls the CryoAdmin for any updates of the alert collections (also called alert definitions). CryoAdmin knows which (Cryo)Relay requests this update by looking at the unique API-key it uses to authenticate itself. CryoAdmin then returns a list of alert collection updates (if any) for the fridges assigned to this relay. If a fridge has no updates, no data for this fridge will be send.

The data returned by the CryoAdmin is a list of updates, with each item an update for a fridge. Each fridge assigned to this relay occurs at most once in the response. The CryoRelay application then pushes each update to the CryoAgent installed on a fridge. This request either return succesfully or not. Finally, the success or failure of the update for a fridge is send to CryoAdmin. If the update for a fridge failed, it will be tried again when CryoRelay polls again.

The data returned by CryoAdmin should have the following structure:

```json
[
    {
        "url": <some_address>,
        "update_uuid": <some_uuid>,
        "alerts": [
            {
                "alert_name": ...,
                "unit": ...,
                "default_enabled": ...,
                "bounds": None
            },
            {
                "alert_name": ...,
                "unit": ...,
                "default_enabled": ...,
                "bounds": {
                    "upper": {
                        "default_enabled": ...,
                        "default_value":...,
                    },
                    "lower": {
                        "default_enabled": ...,
                        "default_value": ...,
                    }
                }
            },
            {
                ...
            }
        ]
    },
    {
        ...
    }
]
```

## Alerts to Metrics and vice-versa

Because a user of a fridge can modify the settings for each alert rule in the CryoAgent web interface, it is needed to include these settings in the database to query them to know if an alert should be triggered. Because CryoAlert uses Prometheus, it is chosen to include the settings for each alert rule as a metric.

The metric corresponding to an alert rule is always called `cryoalert_<alert_rule_name>_settings`. If contains a label called `parameter` that can take the values

- `enabled`
- `bounds_upper_enabled` (if applicable)
- `bounds_upper_value_<unit>` (if applicable)
- `bounds_lower_enabled` (if applicable)
- `bounds_lower_value_<unit>` (if applicable)

Those represent if the alert rule is enabled, whether the upper bound is enabled, what the upper bound is set to, and idem dito for the lower bounds, respectively.

### Example

Assume an alert rule called `alert_temp_mxc_critical` has been defined. CryoAgent converts this alert rule to a metric called `cryoalert_alert_temp_mxc_critical_settings`. To query (using PromQL) the upper bound value for this alert rule and for a specific fridge, the query should look like `cryoalert_alert_temp_mxc_critical_settings{fridge="<fridge_name>", parameter="bounds_upper_value_kelvin"}`. It is these values that can be used to determine if an alert should be triggered or not.
