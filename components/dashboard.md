# Dashboard

For the dashboard, the default option is to use [Grafana](https://grafana.com/oss/grafana/). It is used to visualize data and generate alerts for each fridge for each organization. For further details, see its [documentation](https://grafana.com/docs/grafana/latest/).
