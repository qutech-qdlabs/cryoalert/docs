# CryoAgent

The CryoAgent is main component of the CryoAlert platform. It serves to safely expose data from a data source. An external application can poll data from the agent to obtain the latest set of data. A Prometheus endpoint is available for scraping the latest datapoints.

It should be noted that CryoAgent was designed for use on Windows machines. While it should be possible to run CryoAgent on other platforms, this is not tested nor documented.

For a more detailed documentation, see the CryoAgent [repository](https://gitlab.tudelft.nl/qutech-qdlabs/cryoalert/cryoagent).
