# CryoRelay

The CryoRelay application is a relatively simple application that collectors and forwards alert-collections to the various fridges connected to the relay on which CryoRelay is installed, when such an alert-collection is updated by a user within CryoAdmin.

For a more detailed documentation, see the CryoRelay [repository](https://gitlab.tudelft.nl/qutech-qdlabs/cryoalert/cryorelay).
