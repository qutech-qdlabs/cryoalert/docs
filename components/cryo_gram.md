# CryoGram

The CryoGram application represents a Telegram chat bot called 'CryoAlertBot'. It allows for a Telegram user sending commands to CryoAlertBot. The most used command is the 'subscribe' command, which allows users to receive an invite link from the CryoAlertBot for the Telegram group chat specific to the fridge they want to subscribe to.

CryoGram makes use of the authentication/authorization endpoint of CryoAdmin to verify if a Telegram user is allowed to subscribe to a specific fridge.

For a more detailed documentation, see the CryoGram [repository](https://gitlab.tudelft.nl/qutech-qdlabs/cryoalert/cryogram).
