# Prometheus

[Prometheus](https://prometheus.io) is an open-source monitoring solution. It provides an industry standard method for retrieving and storing data. See its documentation for more details.

Within the CryoAlert Platform, Prometheus is used in two different modes; server mode and agent mode. The agent mode is only used in the deployment option with segregated networks (see [here](../deployment_options/advanced_segregated.md)). In all other modes, the server variant also scrapes the data.

## Server

By default Prometheus will run in server mode. When the server also scrapes data from CryoAgents, it should be configured for service discovery. See the [section](prometheus.md#service-discovery) on that topic for further details. When an agent is present, the Prometheus server will have no further configuration other than those provided by flags.

When an agent writes data to a central Prometheus server, the latter should have its ['remote write receiver'](https://prometheus.io/docs/prometheus/latest/configuration/configuration/#remote_write) enabled with the '--web.enable-remote-write-receiver' flag when starting the Prometheus executable.

For additional security, it is recommended to add authentication to the Prometheus server. This can be done by enabling it using its '--web.config.file=/etc/prometheus/web.yml' flag. This requires a file names 'web.yaml' to be present. The file should contain the following snippet:

```yaml
basic_auth_users:
  <username>: <hashed_password>
```

The hashed password can be created from a plain-text password using the following python script:

```python
import getpass
import bcrypt

password = getpass.getpass("password: ")
hashed_password = bcrypt.hashpw(password.encode("utf-8"), bcrypt.gensalt())
print(hashed_password.decode())
```

## Agent

The same executable than runs Prometheus in server mode, can be configurated to run in agent mode by enabling this feature using the '--enable-feature=agent' flag. Furthermore, it should be configured for service discovery. See the [section](prometheus.md#service-discovery) on that topic for further details.

Note that the 'agent' feature flag does no longer exist on Prometheus Server version 3. Use the Long-Term Support (LTS) version 2.53 to use this feature.

Additionally, it should be configured to write the data it has scraped from CryoAgents to the Prometheus server. The following snippet should be added to the configuration file:

```yaml
remote_write:
  - url: 'http://<prometheus_server_ip>/api/v1/write'
    basic_auth:
      username: <username>
      password: <password>
```

Note that the `basic_auth` field is only necessary when using authentication on the server. For further details, see its [documentation](https://prometheus.io/docs/prometheus/latest/configuration/configuration/#remote_write).

## Service Discovery

It is possible to use two options for service discovery; via its configuration file, or via CryoAdmin. When CryoAdmin is not used within your deployment, the configuration file option should be used. When CryoAdmin is used, you can choose which option to use, although obviously in that case the configuration file option is the most manual, and therefore time consuming, option.

It should be noted that other service discovery methods exists within Prometheus, but are not actively supported by CryoAlert.

### Via Configuration

For using the (configuration) file based option, the following yaml snippet should be added to the configuration file:

```yaml
file_sd_config:
  - targets:
      [<host1>, <host2>, ...]
    labels:
      <label1>: <value1>
      <label2>: <value2>
      ...
```

For further details, see its [documentation](https://prometheus.io/docs/prometheus/latest/configuration/configuration/#file_sd_config).

### Via CryoAdmin

CryoAdmin provides an endpoint designed to work with the http service discovery that Prometheus provides. In this case, the following yaml snippet should be added to the configuration file:

```yaml
http_sd_configs:
  - url: '<url>/api/v1/sd/'
    authorization:
      type: Api-Key
      credentials: <api-key>
```

For further details, see its [documentation](https://prometheus.io/docs/prometheus/latest/configuration/configuration/#http_sd_config). The api-key can be obtained from CryoAdmin.

## Alternatives

Since the development of CryoAlert, a new tool from Grafana has become available; [Mimir](https://grafana.com/oss/mimir/). It is fully compatible with Prometheus, but allows for multi-tenancy, which is something Prometheus does not provide. This allows for true seperation of data for the different organizations. It also is designed to be scalable, which is also something Prometheus does not do well.

You can use Mimir instead of Prometheus in your deployment. This has not been tested however.
