# CryoAlert Components

The CryoAlert Platform consists of multiple components. Below, the various components are listed. It should be noted that not all components are needed for every [deployment option](../deployment_options/index.md).

## Components

- [CryoAgent](cryo_agent.md)
- [Prometheus](prometheus.md)
- [Dashboard](dashboard.md)
- [CryoAdmin](cryo_admin.md)
- [CryoGram](cryo_gram.md)
- [CryoRelay](cryo_relay.md)
