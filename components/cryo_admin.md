# CryoAdmin

The CryoAdmin application allows for central management of various settings used by multiple components of the CryoAlert Platform. Additionally, it contains endpoints for Prometheus' service discovery, retrieval of alert rule updates, and authentication/authorization for CryoGram.

For a more detailed documentation, see the CryoAdmin [repository](https://gitlab.tudelft.nl/qutech-qdlabs/cryoalert/cryoadmin).
