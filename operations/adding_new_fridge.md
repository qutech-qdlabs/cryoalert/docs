# Adding a new Fridge

## 1. Install CryoAgent

To add a new fridge is equivalent to installing CryoAgent on a fridge PC. To learn how to do so, see the [documentation](https://gitlab.tudelft.nl/qutech-qdlabs/cryoalert/cryoagent/-/blob/main/docs/installation.md) for this step.

## 2. Register Fridge

Registering the Fridge on CryoAdmin is the next step. To learn how to do so, see the [documentation](https://gitlab.tudelft.nl/qutech-qdlabs/cryoalert/cryoadmin/-/blob/main/docs/usage/new_fridge.md) for this step.

## 3. Create Telegram Group Chat

Contact webmaster@qutech.support to request a Telegram group chat for your fridge.

## 4. Register Telegram Group Chat

Registering a Telegram group chat for a fridge is the final step. To learn how to do so, see the [documentation](https://gitlab.tudelft.nl/qutech-qdlabs/cryoalert/cryoadmin/-/blob/main/docs/usage/new_fridge_group_chat.md) for this step.