# Adding a new Alert

Adding a new alert, including corresponding alert rules, is a relatively involved process because certain names need to match to make the system work, while at the same time there is no enforcement/direct feedback when a misconfiguration occured. Nevertheless, an attempt at explaining the various parts is made in this document.

However, some assumptions need to be made. While one is free to choose a dashboard technology of choice, as explained in the components [documentation](../components/dashboard.md), here it is assumed use is made of Grafana, which is the dashboard technology used at QuTech and therefore the default.

Additionally, to fully understand how each CryoAgent gets its assigned alert rules, please read the [alert rule's data flow](../data_flow/alert_rules.md) documentation first.

## 1. Designing the Alert

### Questions

Perhaps it is trivial to mention, but the first step to adding an alert is to think very clearly about it. Questions that you should consider are:

- How do I get the data for the alert trigger?
- Is this data available?
- What are sane lower/upper bounds, if applicable?
- What should the alert be called?

Once you have good answers to the above questions, you can commence with adding the new alert.

### Naming Conventions

The naming convention is really trivial; it should be prefixed with `alert_`. However, because use is made of Prometheus metrics (which alert values are converted to, see [here](../data_flow/alert_rules.md#alerts-to-metrics) for the details), some additional restrictions do apply, particularly which symbols are allowed in a metric name. As explained in this [document](https://prometheus.io/docs/concepts/data_model/), the metric name should match the regex `[a-zA-Z_:][a-zA-Z0-9_:]*`. This is important to remember, because CryoAgent automatically enforces metric names. This could mean that an expected alert name might be changed, without any (immediate) feedback, in order to enforce the metric name restrictions.

### Example

For pedagogical clarity, lets work with some examples that match closely with what is used at QuTech. First, an alert with an upper bound;

```py
alert_name: alert_temp_mxc_critical
upper_bound:
    default_enabled: True
    default_value: 500
    unit: mK
```

Secondly, an alert without any bounds;

```py
alert_name: alert_compressor_on
```

The interpertation in this case is that the data corresponding to this alert is a boolean value (in this case "compressor_on"). The reason that an alert rule is still needed in this case, is to enable/disable the alert rule when the fridge users want to.

Note that the values for the upper/lower bound are merely default settings. These can be adjusted in the [CryoAgent web interface](https://gitlab.tudelft.nl/qutech-qdlabs/cryoalert/cryoagent/-/blob/main/docs/usage/alerts.md).

## 2. Adding Alert Rule in CryoAdmin

See the CryoAdmin [documentation](https://gitlab.tudelft.nl/qutech-qdlabs/cryoalert/cryoadmin/-/blob/main/docs/usage/new_alert_rule.md) for the steps to create a new alert rule and adding it to an alert collection. The example is continued there.

## 3. Adding alert in Dashboard

Again, the reader is reminded that in this document the assumption is made that Grafana is used as the dashboard technology. If that is the case, you can read its [documentation](https://grafana.com/docs/grafana/latest/alerting/fundamentals/) about how to add alert rules within Grafana. Additionally, because the data is stored within a Prometheus server, read its [PromQL](https://prometheus.io/docs/prometheus/latest/querying/basics/) documentation to learn about its query language.

For further details on how the alert settings for a specific fridge and alert rule can be queried, see the [alert rule data flow documentation](../data_flow/alert_rules.md#alerts-to-metrics-and-vice-versa). The example is continued there.
