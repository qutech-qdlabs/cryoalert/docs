# Adding a new Organization

There are only 2 places where the concept of an organization is used; CryoAdmin and Grafana (dashboard). Note that the link between the two is not enforced, nor checked. It is simply used an organizational seperation between groups who should not see eachothers data/users/configuration. However, it is good practice to keep the naming consistent between the two, to avoid confusion.

## CryoAdmin

How to add an organization is documented in the CryoAdmin [documentation](https://gitlab.tudelft.nl/qutech-qdlabs/cryoalert/cryoadmin/-/blob/main/docs/usage/new_org.md).

## Grafana

How to add organizations in Grafana and how to use them can be [here](https://grafana.com/docs/grafana/latest/administration/organization-management/).
