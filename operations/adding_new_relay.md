# Adding a new Relay

It takes multiple steps, across multiple components and hardware, to add a functional relay. This document describes the steps across these components that are needed.

## 1. Registration

### Relay

Registering the relay PC on CryoAdmin is the first step. To learn how to do so, see the [documentation](https://gitlab.tudelft.nl/qutech-qdlabs/cryoalert/cryoadmin/-/blob/main/docs/usage/new_relay.md) for this step.

### API-Key

Once the relay has been registered, it is possible to create an API-Key for this relay. To learn how to do so, see the [documentation](https://gitlab.tudelft.nl/qutech-qdlabs/cryoalert/cryoadmin/-/blob/main/docs/usage/new_api_key.md) for this step. Note that this API-Key is needed in the next step, so be sure to save it!

## 2. Installation

### Prometheus (Agent)

Install the [Prometheus Agent](../components/prometheus.md) application on the designated relay PC.

### CryoRelay

Install the [CryoRelay](../components/cryo_relay.md) application on the designated relay PC.
