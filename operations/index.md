# Operations Documentation

For certain administrative tasks, more than one component is involved. For these tasks, seperate documentation is made here.

## Chapters

- [Adding a new organization](adding_new_org.md)
- [Adding a new relay](adding_new_relay.md)
- [Adding a new fridge](adding_new_fridge.md)
- [Adding a new user (including Telegram account)](adding_new_user.md)
- [Adding a new alert](adding_new_alert.md)
