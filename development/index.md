# General Development Notes

For all custom components within the CryoAlert platform, some general development rules have been defined. Please read through this documents carefully before starting developing.

## Chapters

- [Git Flow and Branching](gitflow.md)
