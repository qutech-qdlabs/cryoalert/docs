# Git Flow

For all custom components within the CryoAlert platform, some general development rules have been defined, mainly regarding the git flow and branching/merging strategies. The git flow is chosen to be as simple as possible; Only a main branch with feature/bugfix branches spawning from it when appropriate. This flow is visualized below.

![gitflow](../img/gitflow.png)

## Branching Strategy

When adding a new feature, or fixing a bug, a new branch should be created from the head of the main branch. Such a feature/bugfix branch should only add/fix a specific item. Branches with many additions/fixes will **not** be merged.

## Merge Strategy

For merging, the general policy is to merge often and quickly after all tests of the branch have succeeded and the merge request has been reviewed by another person.

## Publish Strategy

For publishing, the general policy is to publish often and quickly, after a reviewer has reviewed the code and deployed and tested it locally (if applicable). Once a week is estimated to be sufficient, as there will most likely be some security patches of dependencies available by then.
